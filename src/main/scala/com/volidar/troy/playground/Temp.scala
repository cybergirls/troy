package com.volidar.troy.playground

import scala.io.StdIn

object Temp {
  def main(args: Array[String]): Unit = {
    var i = 1
    while (i <= 7){
      if(1 <= 7){
        print(i + ", ")
      }
      i = i + 1
    }
    println(i)

    i = 1
    while(i <= 8){
      if(i != 1) print(", ")
      print(i)
      i += 1
    }
    println

    i = 1
    while(i <= 8){
      print(i)
      if(i != 8) print(", ")
      i += 1
    }
    println
  }
}
