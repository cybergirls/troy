package com.volidar.troy.playground

import scala.io.StdIn

object TicTacToe {
  // + Data
  // + Display
  // + Data modification
  // + Refactoring
  // + Validation
  // - Win cheak
  // _ Human interface
  // - Computer logic
  // - Who is who

  def changeDesk(desk: List[List[String]], rowIndex: Int, columnIndex: Int, value: String): List[List[String]] = {
    val rowToChange = desk(rowIndex)
    val changedRow = rowToChange.updated(columnIndex, value)
    val changedDesk = desk.updated(rowIndex, changedRow)
    changedDesk
  }

  def printRow(row: List[String]): Unit = println(row(0) + "|" + row(1) + "|" + row(2))

  def printDesk(desk: List[List[String]]): Unit = {
    printRow(desk(0))
    println("-+-+-")
    printRow(desk(1))
    println("-+-+-")
    printRow(desk(2))
  }

  def doPlayerTurn(desk: List[List[String]], playerName: String): List[List[String]] = {
    var provenLine = false
    var rowIndex = -1
    var columnIndex = -1
    while(!provenLine) {
      val line = StdIn.readLine(playerName + " >")
      val items = line.split(",").toList
      if(items.length == 2) {
        rowIndex = items(0).toInt
        columnIndex = items(1).toInt
        if(rowIndex >= 0 && rowIndex <= 2 && columnIndex <= 2 && columnIndex >= 0) {
          val rowToTake = desk(rowIndex)
          val cell = rowToTake(columnIndex)
          if(cell == " ") {
            provenLine = true
          }
        }
      }
    }
    val newBoard = changeDesk(desk, rowIndex, columnIndex, playerName)
    newBoard
  }

  def winRow(desk: List[List[String]], rowIndex: Int, playerName: String): Boolean = {
    val cell1 = desk(rowIndex)(0)
    val cell2 = desk(rowIndex)(1)
    val cell3 = desk(rowIndex)(2)

    cell1 == playerName && cell2 == playerName && cell3 == playerName
  }
  def winColumn(desk: List[List[String]], columnIndex: Int, playerName: String): Boolean = {
    val cell1 = desk(0)(columnIndex)
    val cell2 = desk(1)(columnIndex)
    val cell3 = desk(2)(columnIndex)

    cell1 == playerName && cell2 == playerName && cell3 == playerName
  }
  def winMainDiagonal(desk: List[List[String]], playerName: String): Boolean = {
    val cell1 = desk(0)(0)
    val cell2 = desk(1)(1)
    val cell3 = desk(2)(2)

    cell1 == playerName && cell2 == playerName && cell3 == playerName
  }
  def winSecondaryDiagonal(desk: List[List[String]], playerName: String): Boolean = {
    val cell1 = desk(0)(2)
    val cell2 = desk(1)(1)
    val cell3 = desk(2)(0)

    cell1 == playerName && cell2 == playerName && cell3 == playerName
  }
  def winAnything(desk: List[List[String]], playerName: String): Boolean = {
    val wonRow1: Boolean = winRow(desk, 0, playerName)
    val wonRow2: Boolean = winRow(desk, 1, playerName)
    val wonRow3: Boolean = winRow(desk, 2, playerName)
    val wonAnyRow = wonRow1 || wonRow2 || wonRow3

    val wonColumn1: Boolean = winColumn(desk, 0, playerName)
    val wonColumn2: Boolean = winColumn(desk, 1, playerName)
    val wonColumn3: Boolean = winColumn(desk, 2, playerName)
    val wonAnyColumn = wonColumn1 || wonColumn2 || wonColumn3

    val wonMainDiagonal: Boolean = winMainDiagonal(desk, playerName)
    val wonSecondaryDiagonal: Boolean = winSecondaryDiagonal(desk, playerName)
    val wonAnyDiagonal: Boolean = wonMainDiagonal || wonSecondaryDiagonal

    wonAnyRow || wonAnyColumn || wonAnyDiagonal
  }
  def main(args: Array[String]): Unit = {
    //var desk: List[List[String]] =
    //  """000
    //    |XXX
    //    |0 X""".stripMargin.lines.toList.map(_.toList.map(_.toString))

    var desk = List(
      List(" ", " ", " "),
      List(" ", " ", " "),
      List(" ", " ", " ")
    )

    var player = "X"
    printDesk(desk)
    var whoTurn = 1
    while(whoTurn <= 9) {
      desk = doPlayerTurn(desk, player)
      if(player == "X"){
        player = "0"
      }
      else{
        player = "X"
      }
      printDesk(desk)
      whoTurn = whoTurn + 1
    }
    println("YAY!!! YOU MADE IT!!!")

    //var desk = List(
    //  List(" ", " ", " "),
    //  List(" ", " ", " "),
    //  List(" ", " ", " ")
    //)
    //
    //printDesk(desk)
    //var whoTurn = 1
    //while(whoTurn <= 9) {
    //  desk = doPlayerTurn(desk, "X")
    //  printDesk(desk)
    //
    //  desk = doPlayerTurn(desk, "O")
    //  printDesk(desk)
    //
    //  whoTurn = whoTurn + 1
    //}
    //if()
    //println("YAY!!! YOU MADE IT!!!")
  }
}
