package com.volidar.troy.face

import com.volidar.troy.core.Odysseus

import scala.io.StdIn

object Fraction {
  def main(args: Array[String]): Unit = {
    print("Numerator: ")
    var n = StdIn.readLine().toInt
    Odysseus.fireAndForget()
    print("Denomenator: ")
    var d = StdIn.readLine().toInt
    var i = 2
    while(n >= i || d >= i) {
      if(n % i == 0 && d % i == 0) {
        n = n / i
        d = d / i
      } else {
        i = i + 1
      }
    }
    println(n)
    println("--")
    println(d)
  }
}
