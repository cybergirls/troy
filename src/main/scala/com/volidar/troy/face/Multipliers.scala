package com.volidar.troy.face

import com.volidar.troy.core.Odysseus

import scala.io.StdIn

object Multipliers {
  def main(args: Array[String]): Unit = {
    Odysseus.fireAndForget()
    println("Please enter a number!")
    print("> ")
    var n = StdIn.readLine().toInt
    var d = 2
    while(n != 1) {
      if(n % d == 0) {
        print(d + ",")
        n = n / d
      } else {
        d = d + 1
      }
    }
    println("1")
  }
}
