package com.volidar.troy.face

object Counter {
  def main(args: Array[String]): Unit = {
    var z = 1
    while(z <= 10) {
      var n = 1
      while(n <= 10) {
        print(n + " ")
        n = n + 1
      }
      println()
      z = z + 1
    }
  }

}
