val zScalaVersion = "2.12.5"
val zAkkaVersion = "2.5.11"

addCommandAlias("ep", "~exportedProducts")
addCommandAlias("ept", "~;exportedProducts;test:exportedProducts")
addCommandAlias("crep", ";clean;reload;exportedProducts")

name := """troy-angela"""

version := "1.0"

scalaVersion := zScalaVersion

scalacOptions in ThisBuild ++= Seq(
  "-language:postfixOps",
  "-deprecation",
  "-unchecked",
  "-feature"
)
