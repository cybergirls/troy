import sbt.Keys._
import sbt._
import sbtassembly.AssemblyPlugin.autoImport._

mainClass in assembly := None

assemblyJarName in assembly := "assembly.jar"

assemblyMergeStrategy in assembly := {
  //case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
  //case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
  case "META-INF/io.netty.versions.properties" => MergeStrategy.first
  case "gatling-version.properties" => MergeStrategy.first
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}

val distrib = TaskKey[File]("distrib", "Creates a distributable zip file.")

distrib := {
  val fatJar: File = assembly.value
  val baseDir = baseDirectory.value
  val target = Keys.target.value
  val name = Keys.name.value
  val version = Keys.version.value

  val zipFile = target / s"$name.zip"
  IO.delete(zipFile)

  val srcMain = baseDir / "src" / "main"
  val binEntries = IO.listFiles(srcMain / "bin").map(f => f -> s"bin/${f.getName}")
  val confEntries = IO.listFiles(srcMain / "resources").map(f => f -> s"conf/${f.getName}")
  val allZipEntries = (fatJar -> s"lib/${fatJar.getName}") :: /*confEntries.toList ::: */binEntries.toList

  IO.zip(allZipEntries, zipFile)
  zipFile
}
